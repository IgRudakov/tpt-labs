const RULES = [{
        name: 'I',
        arrow: '->',
        right: 'ifS'
    },
    {
        name: 'S',
        arrow: '->',
        right: '(AK)'
    },
    {
        name: 'A',
        arrow: '->',
        right: 'DR'
    },
    {
        name: 'R',
        arrow: '->',
        right: 'CR|DR|0R|$'
    },
    {
        name: 'D',
        arrow: '->',
        right: 'a|b|c|z|_'
    },
    {
        name: 'C',
        arrow: '->',
        right: '1|2|3|4|5|6|7|8|9'
    },
    {
        name: 'K',
        arrow: '->',
        right: 'BA|$'
    },
    {
        name: 'B',
        arrow: '->',
        right: '>|<|==|!='
    },

];
let innerTable = document.createElement("tbody");
let table = document.createElement("table");
let headerTab = document.createElement("thead");
let th1 = document.createElement('th');
let th2 = document.createElement('th');
let th3 = document.createElement('th');

function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}

function createTable() {
    table.id = 'table';
    th3.innerText = 'N';
    th1.innerText = 'rule';
    th2.innerText = 'right';
    document.getElementById("rules").appendChild(table);
    headerTab.appendChild(th3);
    headerTab.appendChild(th1);
    headerTab.appendChild(th2);
    table.appendChild(headerTab);
    removeAllChildNodes(innerTable);
    table.appendChild(innerTable);
}

const rulesDiv = document.getElementById('rules');

function rulesToPage(ruls, into) {
    const header = document.createElement('h1');
    header.innerText = 'правила';
    into.appendChild(header);
    createTable();
    for (let i = 0; i < ruls.length; i++) {
        let tr = document.createElement("tr");
        let tdN = document.createElement("td");
        tdN.innerText = i + 1;
        let tdLex = document.createElement("td");
        tdLex.innerText = ruls[i].name + ' ' + ruls[i].arrow;
        let tdTok = document.createElement("td");
        tdTok.innerText = ruls[i].right;
        innerTable.appendChild(tr);
        tr.appendChild(tdN);
        tr.appendChild(tdLex);
        tr.appendChild(tdTok);
    }
    return into;
}
rulesToPage(RULES, rulesDiv);

function changeRight(array) {
    for (let i = 0; i < array.length; i++) {
        let ruleR = array[i].value.match(/R$/);
        let ruleA = array[i].value.match(/A$/);
        let index = '';
        if (ruleR) {
            index = array[i].value.indexOf(ruleR)
            array[i].value = array[i].value.slice(0, index);
        } else if (ruleA) {
            index = array[i].value.indexOf(ruleA)
            array[i].value = array[i].value.slice(0, index);
        }
    }
    return array;
}

function newRules(rules) {
    let arr = [];
    for (let i = 0; i < rules.length; i++) {
        arr.push({
            name: rules[i]['name'],
            right: rules[i]['right'].split('|')
        });
    }
    return arr;
}

function findPersh(array) {
    let pershArr = [];
    for (let i = 0; i < array.length; i++) {
        let newRight = '';
        let currentArr = array[i].right.split('|');
        let len = currentArr.length;
        for (let j = 0; j < len; j++) {
            if (len === 1) {
                if (currentArr[j].match(/[A-Z]+/)) {
                    let index = currentArr[j].indexOf(currentArr[j].match(/[A-Z]/));
                    index == 0 ? newRight = currentArr[j] : newRight = currentArr[j].slice(0, index);
                    pershArr.push({
                        name: 'ПЕРШ(' + `${i+1}` + ')',
                        value: newRight,
                        letter: array[i]['name'],
                    })
                }
            } else {
                pershArr.push({
                    name: 'ПЕРШ(' + `${i+1}.${j+1}` + ')',
                    value: currentArr[j],
                    letter: array[i]['name'],
                })
            }
        }
    }

    changeRight(pershArr);
    let arr = newRules(RULES);

    for (let i = 0; i < pershArr.length; i++) {
        for (let j = 0; j < arr.length; j++) {
            if (arr[j]['name'].indexOf(pershArr[i].value) != -1) {
                pershArr[i].value = arr[j]['right'];
            }
        }
    }

    return pershArr;
}

function justLetters(rules) {
    let arr = [];
    for (let i = 0; i < rules.length; i++) {
        arr.push(rules[i].name);
    }
    return arr;
}

function rightPart(array) {
    let rightMainArr = [];
    for (let i = 0; i < array.length; i++) {
        rightMainArr.push(array[i]['right'].split('|'));
    }
    return rightMainArr;
}

function findSlid(array) {
    const leftArr = justLetters(array);
    let arr = [];
    for (let i = 1; i < leftArr.length; i++) {
        arr.push({
            name: leftArr[i],
            value: slidBySymbol(array, leftArr[i]),
        })
    }
    return arr;
}

function slidBySymbol(array, symbol) {
    let arr = [];
    const rightArr = rightPart(array);
    console.log(rightArr);
    return arr;
}

function findSlid(array) {
    const leftArr = justLetters(array);
    return [{
            name: `СЛІД (${leftArr[1]}) `,
            value: '$'
        },
        {
            name: `СЛІД (${leftArr[2]}) `,
            value: '<, >, ==, !=, )'
        },
        {
            name: `СЛІД (${leftArr[6]}) `,
            value: ')'
        },
        {
            name: `СЛІД (${leftArr[4]}) `,
            value: '1-9, a, b, c, z, _ <, >, ==, !=, ), 0'
        },
        {
            name: `СЛІД (${leftArr[3]}) `,
            value: '<, >, ==, !=, )'
        },
        {
            name: `СЛІД (${leftArr[5]}) `,
            value: '1-9, a, b, c, z, 0, $'
        },
        {
            name: `СЛІД (${leftArr[7]}) `,
            value: 'a, b, c, z, _'
        }
    ]
}

function findVibor(array) {
    let persh = findPersh(array);
    toVibor(persh);
    for (let i = 0; i < persh.length; i++) {
        if (persh[i]['name'] == 'ВИБІР(7.2)') {
            persh[i]['value'] = ')';
        } else if (persh[i]['name'] == 'ВИБІР(4.4)') {
            persh[i]['value'] = ['>', '<', '==', '!=', ')'];
        }
    }
    return persh;
}

function toVibor(array) {
    for (let i = 0; i < array.length; i++) {
        array[i]['name'] = 'ВИБІР' + array[i]['name'].slice(4);
    }
    return array;
}

function createEl(list, arr, num, part1) {
    let el = document.createElement('li');
    el.innerText = num + '= {' + part1 + '}';
    arr.push(num);
    list.appendChild(el);
}

function parcer(value) {
    let pershArr = findPersh(RULES);
    let slidArr = findSlid(RULES);
    let viborArr = findVibor(RULES);
    removeAllChildNodes(document.getElementById('results'));
    let header = document.createElement('h2');
    header.innerText = 'Як діяли';
    header.id = 'resHeader';
    document.getElementById('results').appendChild(header);
    let arr = [];
    const list = document.createElement('ul');
    list.id = 'list';
    document.getElementById('results').appendChild(list);
    switch (value) {
        case 'first':
            for (let i = 0; i < pershArr.length; i++) {
                createEl(list, arr, pershArr[i].name, pershArr[i].value);
            }
            break;
        case 'second':
            for (let i = 0; i < slidArr.length; i++) {
                createEl(list, arr, slidArr[i].name, slidArr[i].value);
            }
            break;
        case 'third':
            for (let i = 0; i < viborArr.length; i++) {
                createEl(list, arr, viborArr[i].name, viborArr[i].value);
            }
            break;
        default:
            document.getElementById('results').style.display = 'none';
            break;
    }
    return arr;
}

function arrToPage(value) {
    document.getElementById('results').style.display = 'block';
    let arr = parcer(value);
    const pRes = document.createElement('p');
    pRes.id = 'chain';
    pRes.innerText = arr;
}