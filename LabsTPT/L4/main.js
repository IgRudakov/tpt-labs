const RULES = [{
        name: 'I',
        arrow: '->',
        right: 'ifS'
    },
    {
        name: 'S',
        arrow: '->',
        right: '(AK)'
    },
    {
        name: 'A',
        arrow: '->',
        right: 'DR'
    },
    {
        name: 'R',
        arrow: '->',
        right: 'CR|DR|0R|$'
    },
    {
        name: 'D',
        arrow: '->',
        right: 'a|b|c|z|_'
    },
    {
        name: 'C',
        arrow: '->',
        right: '1|2|3|4|5|6|7|8|9'
    },
    {
        name: 'K',
        arrow: '->',
        right: 'BA|$'
    },
    {
        name: 'B',
        arrow: '->',
        right: '>|<|==|!='
    },

];

let innerTable = document.createElement("tbody");
let table = document.createElement("table");
let headerTab = document.createElement("thead");
let th1 = document.createElement('th');
let th2 = document.createElement('th');
let th3 = document.createElement('th');

function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}

function createTable() {
    table.id = 'table';
    th3.innerText = 'N';
    th1.innerText = 'rule';
    th2.innerText = 'right';
    document.getElementById("rules").appendChild(table);
    headerTab.appendChild(th3);
    headerTab.appendChild(th1);
    headerTab.appendChild(th2);
    table.appendChild(headerTab);
    removeAllChildNodes(innerTable);
    table.appendChild(innerTable);
}

const rulesDiv = document.getElementById('rules');

//функція виводу правил на екран
function rulesToPage(ruls, into) {
    const header = document.createElement('h1');
    header.innerText = 'правила';
    into.appendChild(header);
    createTable();
    for (let i = 0; i < ruls.length; i++) {
        let tr = document.createElement("tr");
        let tdN = document.createElement("td");
        tdN.innerText = i + 1;
        let tdLex = document.createElement("td");
        tdLex.innerText = ruls[i].name + ' ' + ruls[i].arrow;
        let tdTok = document.createElement("td");
        tdTok.innerText = ruls[i].right;
        innerTable.appendChild(tr);
        tr.appendChild(tdN);
        tr.appendChild(tdLex);
        tr.appendChild(tdTok);
    }
    return into;
}
rulesToPage(RULES, rulesDiv);

//функція, що знаходить ПЕРШ
function findPersh(array) {
    let pershArr = [];
    for (let i = 0; i < array.length; i++) {
        let newRight = '';
        let currentArr = array[i].right.split('|');
        let len = currentArr.length;
        for (let j = 0; j < len; j++) {
            if (len === 1) {
                if (currentArr[j].match(/[A-Z]+/)) {
                    let index = currentArr[j].indexOf(currentArr[j].match(/[A-Z]/));
                    index == 0 ? newRight = currentArr[j] : newRight = currentArr[j].slice(0, index);
                    pershArr.push({
                        name: 'ПЕРШ(' + `${i+1}` + ')',
                        value: newRight,
                        letter: array[i]['name'],
                    })
                }
            } else {
                pershArr.push({
                    name: 'ПЕРШ(' + `${i+1}.${j+1}` + ')',
                    value: currentArr[j],
                    letter: array[i]['name'],
                })
            }
        }
    }

    changeRight(pershArr);
    let arr = newRules(RULES);

    for (let i = 0; i < pershArr.length; i++) {
        for (let j = 0; j < arr.length; j++) {
            if (arr[j]['name'].indexOf(pershArr[i].value) != -1) {
                pershArr[i].value = arr[j]['right'];
            }
        }
    }

    return pershArr;
}

function changeRight(array) {
    for (let i = 0; i < array.length; i++) {
        let ruleR = array[i].value.match(/R$/);
        let ruleA = array[i].value.match(/A$/);
        let index = '';
        if (ruleR) {
            index = array[i].value.indexOf(ruleR)
            array[i].value = array[i].value.slice(0, index);
        } else if (ruleA) {
            index = array[i].value.indexOf(ruleA)
            array[i].value = array[i].value.slice(0, index);
        }
    }
    return array;
}

function newRules(rules) {
    let arr = [];
    for (let i = 0; i < rules.length; i++) {
        arr.push({
            name: rules[i]['name'],
            right: rules[i]['right'].split('|')
        });
    }
    return arr;
}

//функція пошуку ВИБІР
function findVibor(array) {
    let persh = findPersh(array);
    toVibor(persh);
    for (let i = 0; i < persh.length; i++) {
        if (persh[i]['name'] == 'ВИБІР(7.2)') {
            persh[i]['value'] = ')';
        } else if (persh[i]['name'] == 'ВИБІР(4.4)') {
            persh[i]['value'] = ['>', '<', '==', '!=', ')'];
        }
    }
    return persh;
}

function toVibor(array) {
    for (let i = 0; i < array.length; i++) {
        array[i]['name'] = 'ВИБІР' + array[i]['name'].slice(4);
    }
    return array;
}

let vibor = findVibor(RULES);

//функція пошуку лівої частини команди розпізнавача
function asteriks(rules, vibor) {
    let asterik = [];
    for (let i = 0; i < rules.length; i++) {
        for (let j = 0; j < vibor.length; j++) {
            if (rules[i]['name'] === vibor[j]['letter']) {
                if (rules[i]['right'][0].match(/[a-z0-9\(]/g)) {
                    if (Array.isArray(vibor[j]['value'])) {
                        for (let k = 0; k < vibor[j]['value'].length; k++) {
                            asterik.push({
                                f: 'f (s, ',
                                value: vibor[j]['value'][k],
                                comma: ', ',
                                letter: rules[i]['name'],
                                bracket: ')',
                                right: ''
                            });
                        }
                    } else {
                        asterik.push({
                            f: 'f (s, ',
                            value: vibor[j]['value'],
                            comma: ', ',
                            letter: rules[i]['name'],
                            bracket: ')',
                            right: ''
                        });
                    }
                } else {
                    if (Array.isArray(vibor[j]['value'])) {
                        for (let k = 0; k < vibor[j]['value'].length; k++) {
                            asterik.push({
                                f: 'f* (s, ',
                                value: vibor[j]['value'][k],
                                comma: ', ',
                                letter: rules[i]['name'],
                                bracket: ')',
                                right: ''
                            });
                        }
                    } else {
                        asterik.push({
                            f: 'f* (s, ',
                            value: vibor[j]['value'],
                            comma: ', ',
                            letter: rules[i]['name'],
                            bracket: ')',
                            right: ''
                        });
                    }
                }
            }
        }
    }
    return asterik;
}

//функція пошуку правої частини команди розпізнавача
function rightPart(rules) {
    let newArr = [];
    let finalArr = [];
    const sym1 = 'if';
    const sym2 = '(';
    for (let i = 0; i < rules.length; i++) {
        newArr.push({
            letter: rules[i]['name'],
            rules: rules[i]['right']
        });
    }

    matcher(newArr, finalArr);

    replacer(finalArr, sym1);
    replacer(finalArr, sym2);

    reverser(finalArr);

    return finalArr;
}

function matcher(array, newArr) {
    for (let i = 0; i < array.length; i++) {
        if (array[i]['rules'].match(/[a,b,c,z,_,1-9, >, <, ==, =!]/g)) {
            newArr.push({
                letter: array[i]['letter'],
                rules: '$'
            });
        } else {
            newArr.push({
                letter: array[i]['letter'],
                rules: array[i]['rules']
            });
        }
    }
    return newArr;
}

function replacer(array, sym) {
    for (let i = 0; i < array.length; i++) {
        if (array[i]['rules'].includes(sym)) {
            array[i]['rules'] = array[i]['rules'].replace(sym, '');
        }
    }
    return array;
}

function reverser(array) {
    for (let i = 0; i < array.length; i++) {
        array[i]['rules'] = array[i]['rules'].split('').reverse().join('')
    }
    return array;
}

function newRules2(rules) {
    let arr = [];
    let newArr = [];
    for (let i = 0; i < rules.length; i++) {
        arr.push({
            name: rules[i]['name'],
            right: rules[i]['right'].split('|')
        });
    }
    for (let i = 0; i < arr.length; i++) {
        if (arr[i]['right'].length > 2) {
            for (let j = 0; j < arr[i]['right'].length; j++) {
                if (arr[i]['right'][j].match(/[a,b,c,z,_,1-9, >, <, ==, =!]/g)) {
                    newArr.push({
                        name: arr[i]['name'],
                        right: arr[i]['right'][j]
                    });
                } else {
                    newArr.push({
                        name: arr[i]['name'] + [j],
                        right: arr[i]['right'][j]
                    });
                }
            }
        } else {
            newArr.push({
                name: arr[i]['name'],
                right: arr[i]['right'][0]
            });
        }

    }
    return newArr;
}

//функція збирання докупи команди розпізнавача
function allFunc(left, right) {
    for (let i = 0; i < left.length; i++) {
        for (let j = 0; j < right.length; j++) {
            if (left[i]['letter'] === right[j]['letter']) {
                left[i]['right'] = right[j]['rules'];
            } else {
                switch (left[i]['letter']) {
                    case 'R':
                        if (left[i]['value'].match(/[1-9]/g)) {
                            left[i]['right'] = 'RC';
                        } else if (left[i]['value'].match(/[a,b,c,z,_]/g)) {
                            left[i]['right'] = 'RD';
                        } else if (left[i]['value'].match(/0/g)) {
                            left[i]['right'] = 'R0';
                        } else {
                            left[i]['right'] = '$';
                        }
                        break;
                    case 'K':
                        if (left[i]['value'].match(/[>, <, ==, !==]/g)) {
                            left[i]['right'] = 'AB';
                        }
                        break;
                    default:
                        left[i]['right'] = '$';
                        break;
                }
            }
        }
    }
    return left;
}

function addsOn(thatArray) {
    thatArray.push({
        f: 'f (s, ',
        value: ')',
        comma: ', ',
        letter: ') ',
        bracket: ')',
        right: '$'
    });
    thatArray.push({
        f: 'f* (s, ',
        value: '$',
        comma: ', ',
        letter: 'h0 ',
        bracket: ')',
        right: '$'
    });
    return thatArray;
}

function createEl(list, f, value, comma, letter, bracket, right) {
    let el = document.createElement('li');
    el.innerText = f + value + comma + letter + bracket + ' = {s, ' + right + '}';
    list.appendChild(el);
}

function parcer(value) {
    let lefter = asteriks(RULES, vibor);
    let righter = rightPart(newRules2(RULES));
    let newFunc2 = allFunc(lefter, righter);
    let newFunc = addsOn(newFunc2);
    removeAllChildNodes(document.getElementById('results'));
    let header = document.createElement('h2');
    header.innerText = 'Як діяли';
    header.id = 'resHeader';
    document.getElementById('results').appendChild(header);
    let arr = [];
    const list = document.createElement('ol');
    list.id = 'list';
    document.getElementById('results').appendChild(list);
    switch (value) {
        case 'first':
            for (let i = 0; i < newFunc.length; i++) {
                createEl(list, newFunc[i].f, newFunc[i].value,
                    newFunc[i].comma, newFunc[i].letter, newFunc[i].bracket, newFunc[i].right);
            }
            break;
        default:
            document.getElementById('results').style.display = 'none';
            break;
    }
    return arr;
}

function arrToPage(value) {
    document.getElementById('results').style.display = 'block';
    let arr = parcer(value);
    const pRes = document.createElement('p');
    pRes.id = 'chain';
    pRes.innerText = arr;
}