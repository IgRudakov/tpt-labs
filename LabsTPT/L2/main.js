const RULES = [{
        name: 'I',
        arrow: '->',
        right: 'ifS'
    },
    {
        name: 'S',
        arrow: '->',
        right: '(AK)'
    },
    {
        name: 'A',
        arrow: '->',
        right: 'DR'
    },
    {
        name: 'R',
        arrow: '->',
        right: 'CR|DR|0R|$'
    },
    {
        name: 'D',
        arrow: '->',
        right: 'a|b|c|z|_'
    },
    {
        name: 'C',
        arrow: '->',
        right: '1|2|3|4|5|6|7|8|9'
    },
    {
        name: 'K',
        arrow: '->',
        right: 'BA|$'
    },
    {
        name: 'B',
        arrow: '->',
        right: '>|<|==|!='
    },

];
let innerTable = document.createElement("tbody");
let table = document.createElement("table");
let headerTab = document.createElement("thead");
let th1 = document.createElement('th');
let th2 = document.createElement('th');
let th3 = document.createElement('th');

function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}

function createTable() {
    table.id = 'table';
    th3.innerText = 'N';
    th1.innerText = 'rule';
    th2.innerText = 'right';
    document.getElementById("rules").appendChild(table);
    headerTab.appendChild(th3);
    headerTab.appendChild(th1);
    headerTab.appendChild(th2);
    table.appendChild(headerTab);
    removeAllChildNodes(innerTable);
    table.appendChild(innerTable);
}

const rulesDiv = document.getElementById('rules');

function rulesToPage(ruls, into) {
    const header = document.createElement('h1');
    header.innerText = 'правила';
    into.appendChild(header);
    createTable();
    for (let i = 0; i < ruls.length; i++) {
        let tr = document.createElement("tr");
        let tdN = document.createElement("td");
        tdN.innerText = i+1;
        let tdLex = document.createElement("td");
        tdLex.innerText = ruls[i].name + ' ' + ruls[i].arrow;
        let tdTok = document.createElement("td");
        tdTok.innerText = ruls[i].right;
        innerTable.appendChild(tr);
        tr.appendChild(tdN);
        tr.appendChild(tdLex);
        tr.appendChild(tdTok);
    }
    return into;
}
rulesToPage(RULES, rulesDiv);
function createEl(list, arr, num, part1) {
    let el = document.createElement('li');
    el.innerText = num + ') ' + part1;
    arr.push(num);
    list.appendChild(el);
}

function parcer(value) {
    removeAllChildNodes(document.getElementById('results'));
    let header = document.createElement('h2');
    header.innerText = 'Як діяли';
    header.id = 'resHeader';
    document.getElementById('results').appendChild(header);
    let arr = [];
    const list = document.createElement('ul');
    list.id = 'list';
    document.getElementById('results').appendChild(list);
    switch(value) {
        case 'first':
            createEl(list, arr, '1', 'I->ifS');
            createEl(list, arr, ' 2', '->if(AK)');
            createEl(list, arr, ' 3', '->if(DRK)');
            createEl(list, arr, ' 5.1', '->if(aRK)');
            createEl(list, arr, ' 4.4', '->if(aK)');
            createEl(list, arr, ' 7.1', '->if(aBA)');
            createEl(list, arr, ' 8.1', '->if(a>A)');
            createEl(list, arr, ' 3', '->if(a>DR)');
            createEl(list, arr, ' 5.3', '->if(a>cR)');
            createEl(list, arr, ' 4.4', '->if(a>c)');
            break;
        case 'second':
            createEl(list, arr, '1', 'I->ifS');
            createEl(list, arr, ' 2', '->if(AK)');
            createEl(list, arr, ' 3', '->if(DRK)');
            createEl(list, arr, ' 5.1', '->if(aRK)');
            createEl(list, arr, ' 4.4', '->if(aK)');
            createEl(list, arr, ' 7.2', '->if(a)');
            break;
        case 'third':
            createEl(list, arr, '1', 'I->ifS');
            createEl(list, arr, ' 2', '->if(AK)');
            createEl(list, arr, ' 3', '->if(DRK)');
            createEl(list, arr, ' 5.5', '->if(_RK)');
            createEl(list, arr, ' 4.2', '->if(_DRK)');
            createEl(list, arr, ' 5.1', '->if(_aRK)');
            createEl(list, arr, ' 4.2', '->if(_aDRK)');
            createEl(list, arr, ' 5.4', '->if(_azRK)');
            createEl(list, arr, ' 4.2', '->if(_azDRK)');
            createEl(list, arr, ' 5.1', '->if(_azaRK)');
            createEl(list, arr, ' 4.4', '->if(_azaK)');
            createEl(list, arr, ' 7.1', '->if(_azaBA)');
            createEl(list, arr, ' 8.3', '->if(_aza==A)');
            createEl(list, arr, ' 3', '->if(_aza==DR)');
            createEl(list, arr, ' 5.2', '->if(_aza==bR)');
            createEl(list, arr, ' 4.2', '->if(_aza==bDR)');
            createEl(list, arr, ' 5.1', '->if(_aza==baR)');
            createEl(list, arr, ' 4.2', '->if(_aza==baDR)');
            createEl(list, arr, ' 5.4', '->if(_aza==bazR)');
            createEl(list, arr, ' 4.4', '->if(_aza==baz)');
            break;
        default: 
            document.getElementById('results').style.display = 'none';
            break;
    }
    return arr;
}

function arrToPage (value) {
    document.getElementById('results').style.display = 'block';
    let arr = parcer(value);
    const pRes = document.createElement('p');
    pRes.id = 'chain';
    pRes.innerText = arr;
    const headerBody = document.createElement('h2');
    headerBody.id = 'headerBody';
    headerBody.innerText = 'Синтаксичний розбір';
    document.getElementById('results').appendChild(headerBody);
    document.getElementById('headerBody').appendChild(pRes);
}